package com.example.training;

import com.example.training.model.Employee;
import com.example.training.model.Supplier;
import com.example.training.repository.EmployeeRepository;
import com.example.training.repository.SupplierRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TrainingApplication {
	private static final Logger log = LoggerFactory.getLogger(TrainingApplication.class);


	public static void main(String[] args) {

		SpringApplication.run(TrainingApplication.class, args);
		System.out.println("hello");
	}

	@Bean
	public CommandLineRunner demo(SupplierRepository repository){
		return(args)-> {
			for (Supplier student:repository.findAll()){
				log.info(student.toString());
			}
		};
	}

}
