package com.example.training.repository;

import com.example.training.model.Employee;
import com.example.training.model.Factory;
import com.example.training.model.Supplier;
import com.example.training.model.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    public Employee findByVisa(String visa);
    List<Employee> findAll();



}
