package com.example.training.repository;

import com.example.training.model.EmployeeDetailsView;
import com.example.training.model.TrainingView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TrainingViewRepository extends JpaRepository<TrainingView,Long> {

   @Query(value="select DISTINCT t.training_id, e.first_name,e.last_name,e.visa,t.training_name,t.proposed_training,t.training_status,t.start_date,t.end_date,t.duration,t.mandatory,t.license_attributed, tc.training_type, s.supplier_name from factory f right join employee e ON e.factory=f.factory_id left join employeetraining et ON e.employee_id=et.employee_id right join training t ON et.training_id=t.training_id left join supplier s on t.supplier_id=s.supplier_id left join trainingcategory tc on t.trainingcat_id=tc.trainingcat_id ", nativeQuery=true)
    public List<TrainingView> findTrainingDetails();
}
