package com.example.training.repository;

import com.example.training.model.EmployeeDetailsView;
import com.example.training.model.FactoryDetailsView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FactoryDetailsViewRepository extends JpaRepository<FactoryDetailsView,Long> {

    @Query(value = "SELECT employee_id, first_name, last_name,visa from employee where factory=1", nativeQuery = true)
    public List<FactoryDetailsView> findJavaDetails();

    @Query(value = "SELECT employee_id, first_name, last_name,visa from employee where factory=4", nativeQuery = true)
    public List<FactoryDetailsView> findAcademyDetails();

    @Query(value = "SELECT employee_id, first_name, last_name,visa from employee where factory=2", nativeQuery = true)
    public List<FactoryDetailsView> findIpensionDetails();

    @Query(value = "SELECT employee_id, first_name, last_name,visa from employee where factory=3", nativeQuery = true)
    public List<FactoryDetailsView> findNetDetails();

    @Query(value = "SELECT employee_id, first_name, last_name,visa from employee where factory=6", nativeQuery = true)
    public List<FactoryDetailsView> findAdminDetails();
}
