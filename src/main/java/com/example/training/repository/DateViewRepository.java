package com.example.training.repository;

import com.example.training.model.DateView;
import com.example.training.model.EmployeeDetailsView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DateViewRepository extends JpaRepository<DateView,Long> {

    @Query(value="select e.employee_id, e.first_name,e.last_name,e.visa,t.proposed_training,t.training_status,t.start_date,t.end_date, tc.training_type, s.supplier_name from factory f right join employee e ON e.factory=f.factory_id left join employeetraining et ON e.employee_id=et.employee_id right join training t ON et.training_id=t.training_id left join supplier s on t.supplier_id=s.supplier_id left join trainingcategory tc on t.trainingcat_id=tc.trainingcat_id WHERE t.end_date >= CURDATE()", nativeQuery=true)
    public List<DateView> findDateDetails();

}
