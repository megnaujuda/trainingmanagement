package com.example.training.repository;


import com.example.training.model.EmployeeTraining;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeTrainingRepository extends JpaRepository<EmployeeTraining, Long> {

    @Query(value="SELECT * FROM employeetraining", nativeQuery = true)
    List<EmployeeTraining>findAllEmployeeTraining();

}
