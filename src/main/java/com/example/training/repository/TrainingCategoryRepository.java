package com.example.training.repository;

import com.example.training.model.Employee;
import com.example.training.model.TrainingCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TrainingCategoryRepository extends JpaRepository<TrainingCategory,Long> {

    List<TrainingCategory> findAll();
}
