package com.example.training.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "factory")
public class Factory implements Serializable {
    @Id
    @Column(name = "factoryId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long factoryId;

    public Long getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(Long factoryId) {
        this.factoryId = factoryId;
    }

    @Column(name = "factoryName")
    private String factoryName;

    @OneToMany(mappedBy = "factory", cascade = CascadeType.ALL)
    private Set<Employee> employee;

    public Factory() {
    }



    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public Set<Employee> getEmployee() {
        return employee;
    }

    public void setEmployee(Set<Employee> employee) {
        this.employee = employee;
    }

    public Factory(Long factoryId, String factoryName, Set<Employee> employee) {
       this.factoryId = factoryId;
        this.factoryName = factoryName;
        this.employee = employee;
    }
}
