package com.example.training.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "trainingcategory")
public class TrainingCategory implements Serializable {

    @Id
    @Column(name = "trainingcatId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long trainingcatId;

    @Column(name = "trainingType")
    private String trainingType;

    public Long getTrainingcatId() {
        return trainingcatId;
    }

    public void setTrainingcatId(Long trainingcatId) {
        this.trainingcatId = trainingcatId;
    }

    /*
            @Column(name = "softskills")
            private String softSkills;

            @Column(name = "management")
            private String management;

            @Column(name = "others")
            private String others;

            @Column(name = "technical")
            private String technical;
        */
    @OneToMany(mappedBy = "trainingcatId", cascade = CascadeType.ALL)
    private Set<Training> training;


    public TrainingCategory() {
    }



    public String getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(String trainingType) {
        this.trainingType = trainingType;
    }

    /*
        public String getSoftSkills() {
            return softSkills;
        }

        public void setSoftSkills(String softSkills) {
            this.softSkills = softSkills;
        }

        public String getManagement() {
            return management;
        }

        public void setManagement(String management) {
            this.management = management;
        }

        public String getOthers() {
            return others;
        }

        public void setOthers(String others) {
            this.others = others;
        }

        public String getTechnical() {
            return technical;
        }

        public void setTechnical(String technical) {
            this.technical = technical;
        }
    */
    public Set<Training> getTraining() {
        return training;
    }

    public void setTraining(Set<Training> training) {
        this.training = training;
    }
}
