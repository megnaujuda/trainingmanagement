package com.example.training.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "training")
public class Training implements Serializable {


    @Id
    @Column(name = "trainingId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long trainingId;

    @Column(name = "proposedTraining")
    private String proposedTraining;

    @Column(name = "trainingStatus")
    private String trainingStatus;

    @Column(name = "startDate")
    private String startDate;

    @Column(name = "endDate")
    private String endDate;

    @Column(name = "duration")
    private String duration;

    @Column(name = "mandatory")
    private String mandatory;

    @Column(name = "trainingName")
    private String trainingName;

    @Column(name = "licenseAttributed")
    private String licenseAttributed;

    @OneToMany(mappedBy = "trainingId", cascade = CascadeType.ALL)
    private Set<EmployeeTraining> employeeTraining;

    @JoinColumn(name="trainingcatId")
    private Long trainingcatId;

    @JoinColumn(name="supplierId")
    private Long supplierId;




    public Training() {

    }



    public String getProposedTraining() {
        return proposedTraining;
    }

    public void setProposedTraining(String proposedTraining) {
        this.proposedTraining = proposedTraining;
    }

    public String getTrainingStatus() {
        return trainingStatus;
    }

    public void setTrainingStatus(String trainingStatus) {
        this.trainingStatus = trainingStatus;
    }

    public String getStartDate() {
        return startDate;
    }

    public Long getTrainingId() {
        return trainingId;
    }

//    public void setTrainingId(Long trainingId) {
//        this.trainingId = trainingId;
//    }

    public Long getTrainingcatId() {
        return trainingcatId;
    }

    public void setTrainingcatId(Long trainingcatId) {
        this.trainingcatId = trainingcatId;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Training(Long trainingId, String proposedTraining, String trainingStatus, String startDate, String endDate, String duration, String mandatory, String trainingName, String licenseAttributed, Set<EmployeeTraining> employeeTraining, Long trainingcatId, Long supplierId) {
        this.trainingId = trainingId;
        this.proposedTraining = proposedTraining;
        this.trainingStatus = trainingStatus;
        this.startDate = startDate;
        this.endDate = endDate;
        this.duration = duration;
        this.mandatory = mandatory;
        this.trainingName = trainingName;
        this.licenseAttributed = licenseAttributed;
        this.employeeTraining = employeeTraining;
        this.trainingcatId = trainingcatId;
        this.supplierId = supplierId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public Set<EmployeeTraining> getEmployeeTraining() {
        return employeeTraining;
    }

    public void setEmployeeTraining(Set<EmployeeTraining> employeeTraining) {
        this.employeeTraining = employeeTraining;
    }


    public String getLicenseAttributed() {
        return licenseAttributed;
    }

    public void setLicenseAttributed(String licenseAttributed) {
        this.licenseAttributed = licenseAttributed;
    }

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String trainingName) {
        this.trainingName = trainingName;
    }
}
