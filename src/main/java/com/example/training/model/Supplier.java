package com.example.training.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "supplier")
public class Supplier implements Serializable {

    @Id
    @Column(name = "supplierId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long supplierId;

    @Column(name = "supplierName")
    private String supplierName;

    @Column(name = "licenseBought")
    private Long licenseBought;

    @Column(name = "urlAddress")
    private String urlAddress;

    @OneToMany(mappedBy = "supplierId", cascade = CascadeType.ALL)
    private Set<Training> training;

    public Supplier() {
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Long getLicenseBought() {
        return licenseBought;
    }

    public void setLicenseBought(Long licenseBought) {
        this.licenseBought = licenseBought;
    }

    public Set<Training> getTraining() {
        return training;
    }

    public void setTraining(Set<Training> training) {
        this.training = training;
    }

    public Supplier(Long supplierId, String supplierName, Long licenseBought, String urlAddress, Set<Training> training) {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.licenseBought = licenseBought;
        this.urlAddress = urlAddress;
        this.training = training;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }



    public String getUrlAddress() {
        return urlAddress;
    }

    public void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "supplierId=" + supplierId +
                ", supplierName='" + supplierName + '\'' +
                ", licenseBought='" + licenseBought + '\'' +
                ", urlAddress='" + urlAddress + '\'' +
                '}';
    }
}
