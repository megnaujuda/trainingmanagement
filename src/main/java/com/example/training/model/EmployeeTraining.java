package com.example.training.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employeetraining")
public class EmployeeTraining  implements Serializable {
    @Id
    @Column(name = "employee_training_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long employee_training_id;

    @Column
    private String eligibility;

    @Column(name = "trainingIdentified")
    private String trainingIdentified;

    @JoinColumn(name = "employeeId")
    private Long employeeId;

    @JoinColumn(name = "trainingId")
    private Long trainingId;


    public EmployeeTraining() {
    }

    public Long getEmployee_training_id() {
        return employee_training_id;
    }

    public void setEmployee_training_id(Long employee_training_id) {
        this.employee_training_id = employee_training_id;
    }

    public String getEligibility() {
        return eligibility;
    }

    public void setEligibility(String eligibility) {
        this.eligibility = eligibility;
    }

    public String getTrainingIdentified() {
        return trainingIdentified;
    }

    public void setTrainingIdentified(String trainingIdentified) {
        this.trainingIdentified = trainingIdentified;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(Long trainingId) {
        this.trainingId = trainingId;
    }


}
