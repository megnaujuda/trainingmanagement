package com.example.training.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class EmployeeByIdView {
    @Id
    private int employeeId;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String visa;

    @Column
    private String email;

    @Column
    private String employeeStatus;

    @Column
    private String factoryName;

    @Column
    private String eligibility;

    @Column
    private String trainingIdentified;

    @Column
    private String proposedTraining;

    @Column
    private String trainingStatus;

    @Column
    private String startDate;

    @Column
    private String endDate;

    @Column
    private String duration;

    @Column
    private String mandatory;

    @Column
    private String trainingType;


    @Column
    private String supplierName;

    @Column
    private String licenseAttributed;

    public EmployeeByIdView() {
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVisa() {
        return visa;
    }

    public void setVisa(String visa) {
        this.visa = visa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getEligibility() {
        return eligibility;
    }

    public void setEligibility(String eligibility) {
        this.eligibility = eligibility;
    }

    public String getTrainingIdentified() {
        return trainingIdentified;
    }

    public void setTrainingIdentified(String trainingIdentified) {
        this.trainingIdentified = trainingIdentified;
    }

    public String getProposedTraining() {
        return proposedTraining;
    }

    public void setProposedTraining(String proposedTraining) {
        this.proposedTraining = proposedTraining;
    }

    public String getTrainingStatus() {
        return trainingStatus;
    }

    public void setTrainingStatus(String trainingStatus) {
        this.trainingStatus = trainingStatus;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(String trainingType) {
        this.trainingType = trainingType;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getLicenseAttributed() {
        return licenseAttributed;
    }

    public void setLicenseAttributed(String licenseAttributed) {
        this.licenseAttributed = licenseAttributed;
    }
}

