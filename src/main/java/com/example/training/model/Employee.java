package com.example.training.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {
    @Id
    @Column(name = "employeeId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long employeeId;

    @Column(name = "firstName")
    private String firstName;

    @Column(name="lastName")
    private String lastName;

    @Column(name="visa")
    private String visa;

    @Column(name="email")
    private String email;

    @Column(name="employeeStatus")
    private String employeeStatus;


    @JoinColumn(name="factoryId")
    private int factory;

    @OneToMany(mappedBy = "employeeId", cascade = CascadeType.ALL)
    private Set<EmployeeTraining> employeeTraining;



    public Employee() {
    }

    public Long getEmployeeId() {
        return employeeId;
    }

//    public void setEmployeeId(int employeeId) {
//        this.employeeId = employeeId;
//    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVisa() {
        return visa;
    }

    public void setVisa(String visa) {
        this.visa = visa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public int getFactory() {
        return factory;
    }

    public void setFactory(int factory) {
        this.factory = factory;
    }

   /* public Set<EmployeeTraining> getEmployeeTraining() {
        return employeeTraining;
    }*/

    public void setEmployeeTraining(Set<EmployeeTraining> employeeTraining) {
        this.employeeTraining = employeeTraining;
    }
}
