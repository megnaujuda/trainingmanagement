package com.example.training.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CompletedView {
    @Id
    private Long trainingId;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String visa;

    @Column(name = "trainingName")
    private String trainingName;

    @Column(name = "trainingStatus")
    private String trainingStatus;

    @Column(name = "startDate")
    private String startDate;

    @Column(name = "endDate")
    private String endDate;

    @Column(name = "duration")
    private String duration;

    @Column(name = "mandatory")
    private String mandatory;

    @Column(name = "licenseAttributed")
    private String licenseAttributed;

    @Column
    private String supplierName;

    @Column
    private String trainingType;

    public CompletedView() {
    }

    public Long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(Long trainingId) {
        this.trainingId = trainingId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVisa() {
        return visa;
    }

    public void setVisa(String visa) {
        this.visa = visa;
    }

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String proposedTraining) {
        this.trainingName = proposedTraining;
    }

    public String getTrainingStatus() {
        return trainingStatus;
    }

    public void setTrainingStatus(String trainingStatus) {
        this.trainingStatus = trainingStatus;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getLicenseAttributed() {
        return licenseAttributed;
    }

    public void setLicenseAttributed(String licenseAttributed) {
        this.licenseAttributed = licenseAttributed;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(String trainingType) {
        this.trainingType = trainingType;
    }
}
