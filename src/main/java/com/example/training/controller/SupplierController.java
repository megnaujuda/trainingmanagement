package com.example.training.controller;

import com.example.training.exception.ResourceNotFoundException;
import com.example.training.model.Supplier;
import com.example.training.repository.SupplierRepository;
import com.example.training.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

import java.util.List;

@RestController
@EnableAutoConfiguration
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value="/api")
public class SupplierController {

    @Autowired
    private SupplierService service;
    private SupplierRepository repository;

    //GET ALL SUPPLIERS
    @GetMapping("/suppliers")
    public List<Supplier> findAll() {
        List<Supplier> supplierList = service.findAll();
        return supplierList;
    }

    //get by ID
    @GetMapping("/suppliers/{id}")
    public ResponseEntity<Supplier> getSupplierById(@PathVariable(value = "id") Long supplierId)
            throws ResourceNotFoundException {
        Supplier supplier = service.findById(supplierId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + supplierId));
        return ResponseEntity.ok().body(supplier);
    }

    //ADD SUPPLIER
    @CrossOrigin(origins = "http://localhost:4200/api/addsupplier")
    @PostMapping(path = "/addsupplier")
    public Supplier registerSupplier(@RequestBody Supplier supplier) throws Exception {
        String supplierName = supplier.getSupplierName();
        if ( supplierName != null && !"".equals(supplierName)) {
            Supplier supplierobj = service.fetchSupplierByName(supplierName);
            if (supplierobj != null) {
                throw new Exception("Supplier " + supplierName + " already exists");
            }
        }
        return service.saveSupplier(supplier);
    }




/*
    //UPDATE SUPPLIER
    @PutMapping("/update/{id}")
    Supplier updateSupplier(@RequestBody Supplier newSupplier, @PathVariable int id) {
        return service.findById(id)
                .map(supplier -> {
                    supplier.setLicenseBought(newSupplier.getLicenseBought());
                    supplier.setSupplierName(newSupplier.getSupplierName());
                    supplier.setUrlAddress(newSupplier.getUrlAddress());
                    return service.saveSupplier(supplier);
                }).orElseGet(() -> {
                    newSupplier.setSupplierId(id);
                    return service.saveSupplier(newSupplier);
                });
    }
*/
    //DELETE SUPPLIER
   /* @DeleteMapping("/suppliers/{id}")
    public Map<String, Boolean> deleteSupplier(@PathVariable(value = "id") Integer supplierId)
            throws ResourceNotFoundException {
        Supplier supplier = service.findById(supplierId)
                .orElseThrow(() -> new ResourceNotFoundException("Supplier not found for this id :: " + supplierId));

        service.delete(supplier);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    } */

    @DeleteMapping("/deletesupplierbyid/{id}")
    public void deleteSupplierById(@PathVariable Long id){
        service.deleteSupplierById(id);
    }
}
