package com.example.training.controller;

import com.example.training.model.FactoryDetailsView;
import com.example.training.service.FactoryDetailsViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FactoryDetailsViewController {

    @Autowired
    private FactoryDetailsViewService service;

    @GetMapping("/getfactorydetails")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<FactoryDetailsView> fetchfactorydetails(){
        return service.fetchFactoryDetails();
    }


    @GetMapping("/getacademydetails")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<FactoryDetailsView> fetchacademydetails(){
        return service.fetchAcademyDetails();
    }


    @GetMapping("/getipensiondetails")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<FactoryDetailsView> fetchipensiondetails(){
        return service.fetchIpensionDetails();
    }


    @GetMapping("/getnetdetails")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<FactoryDetailsView> fetchnetdetails(){
        return service.fetchNetDetails();
    }


    @GetMapping("/getadmindetails")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<FactoryDetailsView> fetchnadmindetails(){
        return service.fetchAdminDetails();
    }
}

