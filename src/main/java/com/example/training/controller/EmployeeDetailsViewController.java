package com.example.training.controller;

import com.example.training.model.EmployeeDetailsView;
import com.example.training.service.EmployeeDetailsViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeDetailsViewController {
    @Autowired
    private EmployeeDetailsViewService service;

    @GetMapping("/getemployeedetails")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<EmployeeDetailsView> fetchemployeedetails(){
        return service.fetchEmployeeDetails();
    }
}
