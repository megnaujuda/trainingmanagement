package com.example.training.controller;

import com.example.training.exception.ResourceNotFoundException;
import com.example.training.model.Employee;
import com.example.training.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@EnableAutoConfiguration
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value="/api")
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    //GET ALL EMPLOYEES
    @GetMapping("/employees")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Employee> fetchUserList() {
        List<Employee> employees = new ArrayList<Employee>();
        employees=service.fetchUserList();
        return employees;
    }

    //ADD EMPLOYEES
    @PostMapping("/addemployee")
    @CrossOrigin(origins = "http://localhost:4200")
    public Employee registerEmployee(@RequestBody Employee employee) throws Exception {
        String visa = employee.getVisa();
        if ( visa != null && !"".equals(visa)) {
            Employee employee1 = service.fetchByVisa(visa);
            if (employee1 != null) {
                throw new Exception("Visa " + visa + " already exists");
            }
        }
        return service.saveEmployee(employee);
    }

    //get by EMPLOYEES ID
    @GetMapping("/getemployeebyid/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId)
            throws ResourceNotFoundException {
        Employee employee = service.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
        return ResponseEntity.ok().body(employee);
    }

    //update employee
    @PutMapping("/updateemployee/{id}")
    public Employee updateemployee(@RequestBody Employee employeeObj){
        return service.saveEmployee(employeeObj);

    }
}
