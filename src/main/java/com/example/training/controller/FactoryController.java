package com.example.training.controller;

import com.example.training.model.Factory;
import com.example.training.service.FactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class FactoryController {

    @Autowired
    private FactoryService service;

    @GetMapping("/getfactorylist")
    public List<Factory> fetchFactoryList(){
        List<Factory> factories=new ArrayList<Factory>();
        factories=service.fetchFactoryList();
        return factories;
    }


}
