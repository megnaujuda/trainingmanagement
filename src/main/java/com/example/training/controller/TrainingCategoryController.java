package com.example.training.controller;

import com.example.training.model.Employee;
import com.example.training.model.TrainingCategory;
import com.example.training.service.TrainingCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value="/api")

public class TrainingCategoryController {

    @Autowired
    private TrainingCategoryService service;

    @GetMapping("/trainingcategory")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<TrainingCategory> fetchTrainingCategory() {
        List<TrainingCategory> trainingCategories = new ArrayList<TrainingCategory>();
        trainingCategories = service.fetchTrainingCategory();
        return trainingCategories;
    }
}

