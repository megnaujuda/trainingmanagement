package com.example.training.controller;


import com.example.training.exception.ResourceNotFoundException;
import com.example.training.model.Employee;
import com.example.training.model.Training;
import com.example.training.model.TrainingView;
import com.example.training.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@EnableAutoConfiguration
@CrossOrigin(origins = "http://localhost:4200")
public class TrainingController {

    @Autowired
    private TrainingService service;


    @GetMapping("/gettraininglist")

    public List<Training> fetchTrainingList(){
        List<Training> trainings=new ArrayList<Training>();
        trainings=service.fetchTrainingList();
        return trainings;
    }


//    //ADD training
    @PostMapping("/addtraining")
    @CrossOrigin(origins = "http://localhost:4200")
    public Training registerTraining(@RequestBody Training training) {
        return service.saveTraining(training);
    }


    //update training
    @PutMapping("/updatetraining/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public Training updatetraining(@RequestBody Training trainingObj){
        return service.saveTraining(trainingObj);
    }


    //get by training ID
    @GetMapping("/gettrainingbyid/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Training> getTrainingById(@PathVariable(value = "id") Long trainingId)
            throws ResourceNotFoundException {
        Training training = service.findById(trainingId)
                .orElseThrow(() -> new ResourceNotFoundException("Training not found for this id :: " + trainingId));
        return ResponseEntity.ok().body(training);
    }
}
