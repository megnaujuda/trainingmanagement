package com.example.training.controller;

import com.example.training.model.EmployeeByIdView;
import com.example.training.service.EmployeeByIdViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeByIdViewController {

    @Autowired
    private EmployeeByIdViewService service;

    @GetMapping("/getemployeeid/{employee_id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<EmployeeByIdView> findempid(@PathVariable Long employee_id){
        return service.findEmployeeDetails(employee_id);
    }
}
