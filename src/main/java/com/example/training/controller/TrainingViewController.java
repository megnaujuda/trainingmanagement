package com.example.training.controller;

import com.example.training.model.TrainingView;
import com.example.training.service.TrainingViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TrainingViewController {

    @Autowired
    private TrainingViewService service;

    @GetMapping("/gettrainingdetails")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<TrainingView> fetchtrainingdetails(){
        return service.fetchTrainingDetails();
    }
}
