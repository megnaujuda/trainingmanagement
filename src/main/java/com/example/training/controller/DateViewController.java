package com.example.training.controller;

import com.example.training.model.DateView;
import com.example.training.model.EmployeeDetailsView;
import com.example.training.service.DateViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DateViewController {

    @Autowired
    private DateViewService service;

    @GetMapping("/getdate")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<DateView> fetchdatedetails(){
        return service.fetchDateDetails();
    }

}
