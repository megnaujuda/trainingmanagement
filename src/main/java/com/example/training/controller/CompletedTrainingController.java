package com.example.training.controller;

import com.example.training.model.CompletedView;
import com.example.training.service.CompletedTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompletedTrainingController {
    @Autowired
    private CompletedTrainingService service;

    @GetMapping("/completed")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<CompletedView> fetchcompleteddetails(){
        return service.fetchCompletedDetails();
    }
}
