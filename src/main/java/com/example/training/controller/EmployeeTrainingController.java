package com.example.training.controller;


import com.example.training.model.EmployeeTraining;
import com.example.training.service.EmployeeTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/api")
public class EmployeeTrainingController {

    @Autowired
    public EmployeeTrainingService service;

@GetMapping("/getemployeetraining")
@CrossOrigin(origins = "http://localhost:4200")
public List<EmployeeTraining> findAllEmployeeTraining(){
    return service.findEmpTraining();
}


    @PostMapping("/addemployeetraining")
    @CrossOrigin(origins = "http://localhost:4200")
    public EmployeeTraining registerEmployeeTraining(@RequestBody EmployeeTraining employeeTraining) {
        return service.saveEmployeeTraining(employeeTraining);
    }

}

