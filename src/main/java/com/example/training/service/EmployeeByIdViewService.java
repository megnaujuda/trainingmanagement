package com.example.training.service;

import com.example.training.model.EmployeeByIdView;
import com.example.training.model.EmployeeDetailsView;
import com.example.training.repository.EmployeeByIdViewRepository;
import com.example.training.repository.EmployeeDetailsViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeByIdViewService {
    @Autowired
    private EmployeeByIdViewRepository repo;

    public List<EmployeeByIdView> findEmployeeDetails(Long employee_id){
        return repo.findEmpId(employee_id);
    }
}