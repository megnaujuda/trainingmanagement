package com.example.training.service;

import com.example.training.model.Factory;
import com.example.training.model.TrainingCategory;
import com.example.training.repository.FactoryRepository;
import com.example.training.repository.TrainingCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainingCategoryService {
    @Autowired
    private TrainingCategoryRepository repo;

    public List<TrainingCategory> fetchTrainingCategory(){
        return repo.findAll();
    }
}
