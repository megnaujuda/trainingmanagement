package com.example.training.service;

import com.example.training.model.DateView;
import com.example.training.model.EmployeeDetailsView;
import com.example.training.repository.DateViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DateViewService {
    @Autowired
    private DateViewRepository repo;

    public List<DateView> fetchDateDetails(){
        return repo.findDateDetails();
    }
}
