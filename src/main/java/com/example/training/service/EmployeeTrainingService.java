package com.example.training.service;

import com.example.training.model.EmployeeTraining;
import com.example.training.repository.EmployeeTrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeTrainingService {

    @Autowired
    private EmployeeTrainingRepository repo;

    public List<EmployeeTraining> findEmpTraining(){
        return repo.findAllEmployeeTraining();
    }

    public EmployeeTraining saveEmployeeTraining(EmployeeTraining employeeTraining) {
        return repo.save(employeeTraining);
    }

}
