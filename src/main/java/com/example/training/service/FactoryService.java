package com.example.training.service;

import com.example.training.model.Factory;
import com.example.training.repository.FactoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FactoryService {
    @Autowired
    private FactoryRepository repo;

    public List<Factory> fetchFactoryList(){
        return repo.findAll();
    }

}
