package com.example.training.service;

import com.example.training.model.EmployeeDetailsView;
import com.example.training.model.FactoryDetailsView;
import com.example.training.repository.EmployeeDetailsViewRepository;
import com.example.training.repository.FactoryDetailsViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FactoryDetailsViewService {
    @Autowired
    private FactoryDetailsViewRepository repo;

    public List<FactoryDetailsView> fetchFactoryDetails(){
        return repo.findJavaDetails();
    }

    public List<FactoryDetailsView> fetchAcademyDetails(){
        return repo.findAcademyDetails();
    }

    public List<FactoryDetailsView> fetchIpensionDetails(){
        return repo.findIpensionDetails();
    }

    public List<FactoryDetailsView> fetchNetDetails(){
        return repo.findNetDetails();
    }

    public List<FactoryDetailsView> fetchAdminDetails(){
        return repo.findAdminDetails();
    }

}
