package com.example.training.service;

import com.example.training.model.TrainingView;
import com.example.training.repository.TrainingViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrainingViewService {

    @Autowired
    private TrainingViewRepository repo;

    public List<TrainingView> fetchTrainingDetails(){
        return repo.findTrainingDetails();
    }

    public Optional<TrainingView> findById(Long trainingId) {
        return repo.findById(trainingId);
    }
}
