package com.example.training.service;

import com.example.training.model.Employee;
import com.example.training.model.EmployeeTraining;
import com.example.training.model.Factory;
import com.example.training.model.Supplier;
import com.example.training.repository.EmployeeRepository;
import com.example.training.repository.EmployeeTrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repo;
    private EmployeeTrainingRepository employeetrainingrepo;




    public Employee fetchByVisa(String visa) {
        return repo.findByVisa(visa);
    }

    public Optional<Employee> findById(Long employeeId) {
        return repo.findById(employeeId);
    }

    public EmployeeService(EmployeeRepository repo, EmployeeTrainingRepository employeetrainingrepo) {
        this.repo = repo;
        this.employeetrainingrepo = employeetrainingrepo;
    }

    public List<Employee> fetchUserList(){
        return repo.findAll();
    }

//    public  ResponseEntity<Object> createEmployee(Employee model, EmployeeTraining model1){
//        Employee employee=new Employee();
//        EmployeeTraining employeetraining=new EmployeeTraining();
//
//        String visa = employee.getVisa();
//        if (StringUtils.isEmpty(visa)) {
//            return ResponseEntity.badRequest().body("Visa is missing");
//        }else{
//            employee.setFirstName(model.getFirstName());
//            employee.setLastName(model.getLastName());
//            employee.setVisa(model.getVisa());
//            employee.setEmail(model.getEmail());
//            employee.setEmployeeStatus(model.getEmployeeStatus());
//            employee.setFactory(model.getFactory());
//        }
//        Employee savedEmployee=repo.save(employee);
//        if(repo.findById(savedEmployee.getEmployeeId()).isPresent()){
//            employeetraining.setEligibility(model1.getEligibility());
//            employeetraining.setTrainingIdentified(model1.getTrainingIdentified());
//            employeetraining.setEmployeeId(model1.getEmployeeId());
//            employeetrainingrepo.save(employeetraining);
//            return ResponseEntity.ok("User Created");
//        }else
//            return ResponseEntity.unprocessableEntity().body("failed creating employee");
//    }


    public Employee saveEmployee(Employee employee) {
        return repo.save(employee);
    }

  /*  public ResponseEntity<Object> createEmployee(Employee model, Factory model1){
        Employee employee=new Employee();
        Factory factory=new Factory();

        employee.setVisa(model.getVisa());
        employee.setFirstName(model.getFirstName());
        employee.setLastName(model.getLastName());
        employee.setEmail(model.getEmail());
        employee.setEmployeeStatus(model.getEmployeeStatus());
        employee.setFactoryId(model.getFactoryId());
        return ResponseEntity.ok("blah bal");
    }
*/



}
