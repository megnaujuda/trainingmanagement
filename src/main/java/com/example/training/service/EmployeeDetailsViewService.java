package com.example.training.service;

import com.example.training.model.EmployeeDetailsView;
import com.example.training.repository.EmployeeDetailsViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeDetailsViewService {

    @Autowired
    private EmployeeDetailsViewRepository repo;

    public List<EmployeeDetailsView>fetchEmployeeDetails(){
        return repo.findEmployeeDetails();
    }
}
