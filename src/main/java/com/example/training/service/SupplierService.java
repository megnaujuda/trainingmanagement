package com.example.training.service;

import com.example.training.model.Factory;
import com.example.training.model.Supplier;
import com.example.training.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupplierService {

    @Autowired
    private SupplierRepository repo;

    public Supplier saveSupplier(Supplier supplier) {
        return repo.save(supplier);
    }

    public Supplier fetchSupplierByName(String supplierName) {
        return repo.findBySupplierName(supplierName);
    }

    public List<Supplier> findAll(){
        return repo.findAll();
    }

    public Optional<Supplier> findById(Long supplierId) {
        return repo.findById(supplierId);
    }

    public String deleteSupplierById(Long id){
        String result;
        try {
            repo.deleteById(id);
            result="String successfully deleted";
        }
        catch (Exception e){
            result="error" ;
        }
        return result;
    }
}

