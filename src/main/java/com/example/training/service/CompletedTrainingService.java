package com.example.training.service;

import com.example.training.model.CompletedView;
import com.example.training.repository.CompletedTrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompletedTrainingService {

    @Autowired
    private CompletedTrainingRepository repo;

    public List<CompletedView> fetchCompletedDetails(){
        return repo.findCompletedDetails();
    }
}
