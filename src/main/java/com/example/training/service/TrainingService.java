package com.example.training.service;

import com.example.training.model.Employee;
import com.example.training.model.Training;
import com.example.training.model.TrainingView;
import com.example.training.repository.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrainingService {
    @Autowired
    private TrainingRepository repo;

    public List<Training> fetchTrainingList(){
        return repo.findAll();
    }

    public Training saveTraining(Training training) {
        return repo.save(training);
    }

    public Optional<Training> findById(Long trainingId) {
        return repo.findById(trainingId);
    }


//    public Training fetchByName(String trainingName) {
//        return repo.findByName(trainingName);
//    }


}
